#!/usr/bin/env python
import os
import logging
import random
import time
import sys

import atlassian_jwt_auth
from coolname import generate_slug
from locust import TaskSet, task
from perfkit.clients import HttpBaseLocust

log = logging.getLogger(__name__)

MIN_WAIT = os.getenv("MIN_WAIT", 1000)
MAX_WAIT = os.getenv("MAX_WAIT", 2000)
ACCOUNT_ID = os.getenv("ACCOUNT_ID", None)

PROJECT_ID_TO_ISSUE_ID_DICT = {
    # https://jops-perf-test.jira-dev.com/projects/IO/incidents/all
    10002: [10004, 10005, 10006, 10007, 10008],
    # https://jops-perf-test.jira-dev.com/projects/IT/incidents/all
    10003: [10009, 10010, 10011, 10012, 10013],
    # https://jops-perf-test.jira-dev.com/projects/IN/incidents/all
    10004: [10014, 10015, 10016, 10017, 10018],
    # https://jops-perf-test.jira-dev.com/projects/IF/incidents/all
    10005: [10019, 10020, 10021, 10022, 10023],
    # https://jops-perf-test.jira-dev.com/projects/NCDNTS/incidents/all
    10006: [10024, 10025, 10026, 10027, 10028]
}

asap_token = None
asap_config = {
    "kid": "local-issuer/1538977485",
    "issuer": "local-issuer",
    "audience": "local-issuer",
    "expiry": 60,
    "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAglJ8M+zLSbh82B1MtDSu31h0p3y9OmdVl/URTXHczeTX4z7O\nORLW8/JUa9yABsrIaWzfeUrfx8Bz+4Kv1kY2/V3SmKia6ea8EVztMx8nNGRM5ZJd\nt9s618h+PQkbcb7O/TnUopBDWwpGEFKMmBgr3IxJVNo3fjtm8FFPIx3SxfRKOQb5\nXNRBPXx7WeK0hDn4HtiOhlslQ+AWKRDHCAHS+YCXgWFYDioyuvjUn6KSS4zQ8GUX\n9qke35FP1wrji+dUZWrK3KO01fBcBYiO24g8JEhmU00XSxVMb+t0AQASlaAN+GkH\nIsT8LLHgHyz0n8agrgZSwp2sVHihmz4AiFVt3QIDAQABAoIBABaX+lR+Pxg+1dYZ\n7kxnvdGW4lKPO7c+ZwZ7RE6q6N8BR7RsFRCsBXwxjRVOGxwSWAB/EZdTtcxJv1Hh\nbF63uZzfgT1aLdFTYVOZSr5MLl3U7cf3Qiw349G5PrmSNKHZtMUOG6E7ZZIJC/CU\nfy93MVOBNs1xL6HA5q3PSusJppsO6Ux/go+7zJ5/Sl/lVx1tgnBjP9jfyH5bHt4y\nScF5b1WrWLyNzT4vWsf9PY/3K5nRaEW5nEa9mn/5t087qIHesjyBh6GklhNbpKwY\nvK1KulDDVnXAT49kihqrYzc4emfByQLzeqtcKnnjxRnXSJKAttfvHAteSHqttV/D\nF3iragECgYEA0TDmtmhc43g6b7VAGu/8IWGMuFczXznxY0L8zBJ1b8gZiQuFYdi0\nowsKq/XQ3d6RnHRcXKsQlM7WwBN4+6OK6fpfrmqk0iNupA2aI+j3yTbwNn2vx9wC\nwYdOsLYS8PcfLiGxld86L3AZ889hzFXhSOHbIM9q9TCX5AQjcvKQlB0CgYEAn3u8\n9wqsiDZ0/dPG5cMuvvLh7zx1BmKIseCwh0oDg7QF+5TZn4JG/W3eQwr1Fq9KYLXa\nF4fByeIhGEgqw4H/OVzxsVmzukcJ/KZKmSLLZkaqiJU/deyGLy3iha21zZKDM0ci\n7M5n7NHnB87/D1+44Yn5Fguo32eDMI58fbp5lMECgYAUmQEOkuYai7sNRsaCIIEt\n9EH74LIxPS5UfMMRgPvJPuWsxdKjWMgoiazKU9D2F7U0t4xkn39pd5hTn1c5R9gk\nw79qGBwUiOKbCq/Tu1uyl1M9ulB1mGc5EQ6hfbwNt/d8/lpTDIDZLOuRQyk5Cne9\nSmfeKi446Ej8bZ/j8JhlzQKBgQCKsa96IBw/MSy4+Y2FjyJ+CPeXeDpFXTeHieG7\nJhC8Xri9O4uufjuCdiPOph7SpYrHZB7QS82p5i/l0AzwjYMJvdyq4Vm9672xrGQ/\nnNTKkzYHJdRUl2dybDMNEi6DIqFa3MhwrGgQudJzJf8GvbYo3jZyh51K+9hJSADX\nzm2hgQKBgHrJRC1VduDrfh14RX+UwtjR5P3Ldsvpaxlev5mtUwRas4ayW8LAy7Aw\nN4EfifmbuQ3DxhDbdwhKRprcyKy0uFNTs1KG1vPGd5U+ONN0CQ16odJIAfU2d7US\n8bsTImmjo//OD5E0Nd40XA4LvVXjocmzKgheuZ7oMKuOwO282pru\n-----END RSA PRIVATE KEY-----\n"
}


def get_timeline_event_url(issue_id):
    return "https://jalapeno.us-east-1.staging.atl-paas.net/api/316fe55f-dcc9-45f4-869c-7295773fab31/issues/%r/timeline" % issue_id


class Tasks(TaskSet):

    @task
    def get_timeline_updates(self):
        project_id, issue_id = get_project_and_issue()
        timeline_url = get_timeline_event_url(issue_id)
        token = create_asap_token(audience='jalapeno')
        response = self.client.get(
            timeline_url,
            headers={"authorization": "Bearer " + token},
            name='listTimelineEvents'
        )
        log.info("Response status code for get_timeline_updates is: (%r)" % response.status_code)

    @task
    def post_timeline_updates(self):
        project_id, issue_id = get_project_and_issue()
        timeline_url = get_timeline_event_url(issue_id)

        log.info("Posting timeline update to project (%r), on the issue (%r)" % (project_id, issue_id))
        token = create_asap_token(audience='jalapeno')
        response = self.client.post(
            timeline_url,
            headers={"authorization": "Bearer " + token},
            json={
                'projectId': project_id,
                'timestamp': int(time.time()),
                'title': generate_slug(4),
                "content": "{\"version\":1,\"type\":\"doc\",\"content\":[{\"type\":\"paragraph\",\"content\":[{\"type\":\"text\",\"text\":\"Posted Content: %r\"}]}]}"
                           % generate_slug(4),
                'type': 'Update',
                'hidden': 'false',
                'sourceDetails': {
                    'externalEventId': 123,
                    'streamhubEventId': 456,
                    'externalLink': {
                        'name': 'Link name',
                        'href': 'www.example.com'
                    }
                },
                'userInfo': {
                    'actor': {
                        "externalId": "bschoone",
                        "atlassianAccountId": ACCOUNT_ID,
                    },
                    'targets': [{
                        "externalId": "bschoone",
                        "atlassianAccountId": ACCOUNT_ID,
                    }],
                },
            },
            name='createTimelineEvent')
        log.info("Response status code for post_timeline_updates is: (%r)" % response.status_code)


class ManageTimeline(HttpBaseLocust):
    if ACCOUNT_ID is None:
        log.error("ACCOUNT_ID was None")
        sys.exit(1)
    task_set = Tasks
    min_wait = MIN_WAIT
    max_wait = MAX_WAIT


def get_project_and_issue():
    global PROJECT_ID_TO_ISSUE_ID_DICT
    project_id, possible_issue_ids = random.choice(list(PROJECT_ID_TO_ISSUE_ID_DICT.items()))
    issue_id = random.choice(list(possible_issue_ids))
    return project_id, issue_id


def create_asap_token(audience=None, issuer=None, expiry=None):
    """Get JSONified asap info from a file and create a token.

    :param string config_file: file name of JSON file
    :param string audience: service token is sent to
    :param string issuer: name of issuing service
    :param int expiry: expiration time in seconds
    :return: An ASAP token
    :rtype: str
    """

    global asap_token
    if asap_token is None:
        log.info("Generating ASAP Token")

        global asap_config
        if audience:
            asap_config['audience'] = audience
        if issuer:
            asap_config['issuer'] = issuer
        if expiry:
            asap_config['expiry'] = expiry

        signer = atlassian_jwt_auth.create_signer(
            asap_config['issuer'],
            asap_config['kid'],
            asap_config['privateKey'],
        )

        asap_token = signer.generate_jwt(
            asap_config['audience'],
            expiry=asap_config['expiry'],
            additional_claims={
                'accountId': ACCOUNT_ID}
        )
    return asap_token
